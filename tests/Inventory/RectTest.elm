module Inventory.RectTest exposing (..)

import Dict exposing (Dict)
import Expect
import Inventory.Location as Loc exposing (..)
import Inventory.Rect as Inventory exposing (..)
import Test exposing (..)



-- Check out https://package.elm-lang.org/packages/elm-explorations/test/latest to learn more about testing in Elm!


all : Test
all =
    describe "inventory.rect"
        [ test "slot insert success" <|
            \_ ->
                Ok Inventory.empty
                    |> Result.andThen (Inventory.insertItem (Loc.Slot "slot") { id = "item", size = ( 99, 99 ) })
                    |> Expect.all
                        [ Expect.ok
                        , Result.map (Inventory.getItem (Loc.Slot "slot")) >> Expect.equal (Ok (Just { id = "item", size = ( 99, 99 ) }))
                        , Result.map (Inventory.getItem (Loc.Slot "slat")) >> Expect.equal (Ok Nothing)
                        ]
        , test "slot insert full-fail" <|
            \_ ->
                Ok Inventory.empty
                    |> Result.andThen (Inventory.insertItem (Loc.Slot "slot") { id = "item", size = ( 99, 99 ) })
                    |> Result.andThen (Inventory.insertItem (Loc.Slot "slot") { id = "item2", size = ( 99, 99 ) })
                    |> Expect.err
        , test "tab insert bounds-fail" <|
            \_ ->
                Inventory.empty
                    |> Inventory.insertTab "tab" ( 3, 3 )
                    |> Inventory.insertItem (Loc.Tab "tab" ( 0, 2 )) { id = "item", size = ( 1, 2 ) }
                    |> Expect.err
        , test "tab insert success" <|
            \_ ->
                Inventory.empty
                    |> Inventory.insertTab "tab" ( 3, 3 )
                    |> Inventory.insertItem (Loc.Tab "tab" ( 0, 1 )) { id = "item", size = ( 1, 2 ) }
                    |> Expect.all
                        [ Expect.ok
                        , Result.map (Inventory.getItem (Loc.Tab "tab" ( 0, 1 ))) >> Expect.equal (Ok (Just { id = "item", size = ( 1, 2 ) }))
                        , Result.map (Inventory.getItem (Loc.Tab "tab" ( 0, 2 ))) >> Expect.equal (Ok (Just { id = "item", size = ( 1, 2 ) }))
                        , Result.map (Inventory.getItem (Loc.Tab "tab" ( 0, 3 ))) >> Expect.equal (Ok Nothing)
                        , Result.map (Inventory.getItem (Loc.Tab "tab" ( 1, 1 ))) >> Expect.equal (Ok Nothing)
                        , Result.map (Inventory.getTab "tab") >> Expect.equal (Ok <| Just <| Dict.fromList [ ( ( 0, 1 ), { id = "item", size = ( 1, 2 ) } ) ])
                        ]
        , test "tab insert collision-fail" <|
            \_ ->
                Inventory.empty
                    |> Inventory.insertTab "tab" ( 3, 3 )
                    |> Inventory.insertItem (Loc.Tab "tab" ( 0, 0 )) { id = "item", size = ( 1, 2 ) }
                    |> Expect.all
                        [ Expect.ok
                        , Result.andThen (Inventory.insertItem (Loc.Tab "tab" ( 0, 0 )) { id = "item", size = ( 1, 2 ) }) >> Expect.err
                        , Result.andThen (Inventory.insertItem (Loc.Tab "tab" ( 0, 1 )) { id = "item", size = ( 1, 2 ) }) >> Expect.err
                        , Result.andThen (Inventory.insertItem (Loc.Tab "tab" ( 1, 0 )) { id = "item", size = ( 1, 2 ) }) >> Expect.ok
                        ]
        , test "slot remove " <|
            \_ ->
                Ok Inventory.empty
                    |> Result.andThen (Inventory.insertItem (Loc.Slot "slot") { id = "item", size = ( 99, 99 ) })
                    |> Result.map (Inventory.removeItem (Loc.Slot "slot"))
                    |> Expect.all
                        [ Expect.ok
                        , Result.map (Inventory.getItem (Loc.Slot "slot")) >> Expect.equal (Ok Nothing)
                        ]
        , test "tab remove" <|
            \_ ->
                Inventory.empty
                    |> Inventory.insertTab "tab" ( 3, 3 )
                    |> Inventory.insertItem (Loc.Tab "tab" ( 0, 1 )) { id = "item", size = ( 1, 2 ) }
                    -- remove from different location than inserted!
                    |> Result.map (Inventory.removeItem (Loc.Tab "tab" ( 0, 2 )))
                    |> Expect.all
                        [ Expect.ok
                        , Result.map (Inventory.getItem (Loc.Tab "tab" ( 0, 1 ))) >> Expect.equal (Ok Nothing)
                        , Result.map (Inventory.getItem (Loc.Tab "tab" ( 0, 2 ))) >> Expect.equal (Ok Nothing)
                        ]
        , describe "swap"
            [ test "slot/slot swap" <|
                \_ ->
                    Ok Inventory.empty
                        |> Result.andThen (Inventory.insertItem (Loc.Slot "a") { id = "item1", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.insertItem (Loc.Slot "b") { id = "item2", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.swapItems (Loc.Slot "a") (Loc.Slot "b"))
                        |> Expect.all
                            [ Expect.ok
                            , Result.map (Inventory.getItem (Loc.Slot "a")) >> Expect.equal (Ok <| Just { id = "item2", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Slot "b")) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            ]
            , test "slot/tab swap" <|
                \_ ->
                    Ok Inventory.empty
                        |> Result.map (Inventory.insertTab "a" ( 3, 3 ))
                        |> Result.andThen (Inventory.insertItem (Loc.Tab "a" ( 0, 0 )) { id = "item1", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.insertItem (Loc.Slot "b") { id = "item2", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.swapItems (Loc.Tab "a" ( 0, 0 )) (Loc.Slot "b"))
                        |> Expect.all
                            [ Expect.ok
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 0 ))) >> Expect.equal (Ok <| Just { id = "item2", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Slot "b")) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            ]
            , test "slot/empty swap" <|
                \_ ->
                    Ok Inventory.empty
                        |> Result.andThen (Inventory.insertItem (Loc.Slot "a") { id = "item1", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.swapItems (Loc.Slot "a") (Loc.Slot "b"))
                        |> Expect.all
                            [ Expect.ok
                            , Result.map (Inventory.getItem (Loc.Slot "a")) >> Expect.equal (Ok Nothing)
                            , Result.map (Inventory.getItem (Loc.Slot "b")) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            ]
            , test "slot/tab non-origin swap" <|
                \_ ->
                    Ok Inventory.empty
                        |> Result.map (Inventory.insertTab "a" ( 3, 3 ))
                        |> Result.andThen (Inventory.insertItem (Loc.Tab "a" ( 0, 0 )) { id = "item1", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.insertItem (Loc.Slot "b") { id = "item2", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.swapItems (Loc.Tab "a" ( 0, 1 )) (Loc.Slot "b"))
                        |> Expect.all
                            [ Expect.ok
                            , Result.map (Inventory.getItem (Loc.Slot "b")) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 1 ))) >> Expect.equal (Ok <| Just { id = "item2", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 2 ))) >> Expect.equal (Ok <| Just { id = "item2", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 0 ))) >> Expect.equal (Ok Nothing)
                            ]
            ]
        , describe "pushItem"
            [ test "emtpy tab too small fail" <|
                \_ ->
                    Inventory.empty
                        |> Inventory.insertTab "a" ( 3, 3 )
                        |> Inventory.pushItem "a" { id = "item1", size = ( 4, 3 ) }
                        |> Expect.err
            , test "empty tab success" <|
                \_ ->
                    Inventory.empty
                        |> Inventory.insertTab "a" ( 3, 3 )
                        |> Inventory.pushItem "a" { id = "item1", size = ( 1, 2 ) }
                        |> Expect.all
                            [ Expect.ok
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 0 ))) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 1 ))) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 2 ))) >> Expect.equal (Ok Nothing)
                            ]
            , test "multiple push success" <|
                \_ ->
                    Inventory.empty
                        |> Inventory.insertTab "a" ( 2, 5 )
                        |> Ok
                        |> Result.andThen (Inventory.pushItem "a" { id = "item1", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item2", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item3", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item4", size = ( 1, 2 ) })
                        -- expected inventory after above pushes:
                        -- -------
                        -- | 1 2 |
                        -- | 1 2 |
                        -- | 3 4 |
                        -- | 3 4 |
                        -- | _ _ |
                        -- -------
                        |> Expect.all
                            [ Expect.ok
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 0 ))) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 1 ))) >> Expect.equal (Ok <| Just { id = "item1", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 1, 0 ))) >> Expect.equal (Ok <| Just { id = "item2", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 1, 1 ))) >> Expect.equal (Ok <| Just { id = "item2", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 2 ))) >> Expect.equal (Ok <| Just { id = "item3", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 3 ))) >> Expect.equal (Ok <| Just { id = "item3", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 1, 2 ))) >> Expect.equal (Ok <| Just { id = "item4", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 1, 3 ))) >> Expect.equal (Ok <| Just { id = "item4", size = ( 1, 2 ) })
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 0, 4 ))) >> Expect.equal (Ok Nothing)
                            , Result.map (Inventory.getItem (Loc.Tab "a" ( 1, 4 ))) >> Expect.equal (Ok Nothing)
                            ]
            , test "tab full fail" <|
                \_ ->
                    Inventory.empty
                        |> Inventory.insertTab "a" ( 2, 5 )
                        |> Ok
                        |> Result.andThen (Inventory.pushItem "a" { id = "item1", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item2", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item3", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item4", size = ( 1, 2 ) })
                        |> Result.andThen (Inventory.pushItem "a" { id = "item5", size = ( 1, 2 ) })
                        |> Expect.equal (Err "inventory is full")
            ]
        ]
