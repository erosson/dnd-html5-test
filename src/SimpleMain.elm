module SimpleMain exposing (main)

import Browser
import Browser.Navigation as Nav
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Inventory.Cursor as Cursor exposing (Cursor)
import Inventory.Location as Loc exposing (ItemId, Location(..), SlotName, TabId)
import Inventory.Simple as Inventory exposing (Inventory)
import Json.Decode as D
import Url exposing (Url)



---- MODEL ----


type alias Model =
    { nav : Nav.Key
    , url : Url
    , inventory : Inventory
    , dragstart : Maybe Location
    , dragover : Maybe Location
    , error : Maybe String
    }


type alias Flags =
    ()


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url nav =
    let
        emptyInventory =
            Inventory.empty
                |> Inventory.insertTab "inv" ( 10, 5 )
                |> Inventory.insertTab "stash" ( 12, 8 )
    in
    ( { nav = nav
      , url = url
      , inventory =
            emptyInventory
                |> Inventory.insertItems
                    [ ( Tab "inv" ( 0, 0 ), "red" )
                    , ( Tab "inv" ( 1, 2 ), "blue" )
                    , ( Tab "inv" ( 3, 3 ), "green" )
                    , ( Tab "stash" ( 8, 4 ), "yellow" )
                    , ( Slot "helm", "purple" )
                    ]
                |> Result.withDefault emptyInventory
      , dragstart = Nothing
      , dragover = Nothing
      , error = Nothing
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = NoOp
    | InvDragStart Location ItemId
    | InvDragEnd
    | InvDragEnter Location
    | InvDragOver Location
    | InvDragLeave Location
    | InvDragDrop Location
    | PushItem
    | PushNextTab Location
    | DestroyItem
    | OnUrlChange Url
    | OnUrlRequest Browser.UrlRequest


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        PushItem ->
            case model.inventory |> Inventory.pushItem "inv" "pink" of
                Err err ->
                    ( { model | error = Just err }, Cmd.none )

                Ok inv ->
                    ( { model | error = Nothing, inventory = inv }, Cmd.none )

        DestroyItem ->
            case model.dragstart of
                Nothing ->
                    ( model, Cmd.none )

                Just loc ->
                    ( { model | inventory = model.inventory |> Inventory.removeItem loc }, Cmd.none )

        InvDragStart loc itemId ->
            ( model |> Cursor.dragStart loc, Cmd.none )

        InvDragEnd ->
            ( model |> Cursor.dragEnd, Cmd.none )

        InvDragEnter loc ->
            ( model |> Cursor.dragEnter loc, Cmd.none )

        InvDragOver loc ->
            ( model |> Cursor.dragOver loc, Cmd.none )

        InvDragLeave loc ->
            ( model |> Cursor.dragLeave loc, Cmd.none )

        InvDragDrop dropLoc ->
            let
                nextModel =
                    case model.dragstart of
                        Nothing ->
                            model

                        Just startLoc ->
                            case model.inventory |> Inventory.swapItems startLoc dropLoc of
                                Err err ->
                                    { model | error = Just err }

                                Ok inv ->
                                    { model | error = Nothing, inventory = inv }
            in
            ( nextModel |> Cursor.dragDrop, Cmd.none )

        PushNextTab startLoc ->
            let
                mTargetTabId =
                    case startLoc of
                        Loc.Tab "inv" _ ->
                            Just "stash"

                        Loc.Tab "stash" _ ->
                            Just "inv"

                        _ ->
                            Nothing
            in
            case mTargetTabId of
                Nothing ->
                    ( model, Cmd.none )

                Just targetTabId ->
                    case model.inventory |> Inventory.quickpushItem startLoc targetTabId of
                        Err err ->
                            ( { model | error = Just err }, Cmd.none )

                        Ok inv ->
                            ( { model | error = Nothing, inventory = inv }, Cmd.none )

        OnUrlChange url ->
            ( { model | url = url }, Cmd.none )

        OnUrlRequest req ->
            ( model
            , case req of
                Browser.Internal url ->
                    Nav.pushUrl model.nav <| Url.toString url

                Browser.External str ->
                    Nav.load str
            )



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    { title = "dnd-html5-test: inventory"
    , body = viewBody model
    }


viewBody : Model -> List (Html Msg)
viewBody model =
    [ viewTab model "inv"
    , viewTab model "stash"
    , viewSlot model "helm"
    , viewSlot model "torso"
    , viewSlot model "weapon"
    , button [ onClick PushItem ] [ text "push item" ]
    , div []
        [ label [] [ text "trash: " ]
        , inventoryBoxOnDrop DestroyItem model (Loc.Slot "trash") []
        ]
    , div [ class "error" ]
        (case model.error of
            Just err ->
                [ code [] [ text "error: ", text err ] ]

            Nothing ->
                []
        )
    ]


viewSlot : Model -> SlotName -> Html Msg
viewSlot model name =
    div [ class "inventory" ] [ label [] [ text name, text ": " ], viewBox model (Slot name) ]


viewTab : Model -> TabId -> Html Msg
viewTab model tabId =
    div [ class "inventory" ] <|
        case Inventory.getTab tabId model.inventory of
            Nothing ->
                []

            Just tab ->
                let
                    ( xMax, yMax ) =
                        tab.bounds
                in
                List.range 0 (yMax - 1)
                    |> List.map
                        (\y ->
                            div [ class "inventory-row" ]
                                (List.range 0 (xMax - 1)
                                    |> List.map
                                        (\x ->
                                            viewBox model (Tab tabId ( x, y ))
                                        )
                                )
                        )


viewBox : Model -> Location -> Html Msg
viewBox model loc =
    inventoryBox model
        loc
        (case Inventory.getItem loc model.inventory of
            Just itemId ->
                [ div
                    [ class "inventory-item"
                    , draggable "true"
                    , style "background" itemId
                    , on "dragstart" <| D.succeed <| InvDragStart loc itemId
                    , on "dragend" <| D.succeed InvDragEnd
                    , onDoubleClick <| PushNextTab loc
                    ]
                    []
                , div [ class "inventory-tooltip" ]
                    [ div [] [ b [] [ text "item id: ", text itemId ] ]
                    , div [] [ text "tooltip demo, ", i [] [ text "weeeee" ] ]
                    ]
                ]

            Nothing ->
                []
        )


inventoryBox : Model -> Location -> List (Html Msg) -> Html Msg
inventoryBox model loc =
    inventoryBoxOnDrop (InvDragDrop loc) model loc


inventoryBoxOnDrop : Msg -> Model -> Location -> List (Html Msg) -> Html Msg
inventoryBoxOnDrop dropMsg model loc =
    div
        [ id <| Loc.toString loc
        , class "inventory-box"
        , class <|
            if model.dragover == Just loc then
                "inventory-dragover"

            else
                "inventory-nodragover"
        , class <|
            if model.dragstart == Just loc then
                "dragging"

            else
                "nodragging"

        -- , on "dragenter" <| D.succeed (Inventory <| Inventory.DragEnter slotId)
        , preventDefaultOn "dragenter" <| D.succeed ( InvDragEnter loc, True )
        , preventDefaultOn "dragover" <| D.succeed ( InvDragOver loc, True )
        , on "dragleave" <| D.succeed (InvDragLeave loc)
        , on "drop" <| D.succeed dropMsg

        -- , preventDefaultOn "drop" <| D.succeed ( Inventory <| Inventory.DragDrop slotId, True )
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        , onUrlChange = OnUrlChange
        , onUrlRequest = OnUrlRequest
        }
