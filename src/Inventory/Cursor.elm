module Inventory.Cursor exposing
    ( Cursor
    , dragStart, dragEnd
    , dragEnter, dragLeave, dragOver, dragDrop
    )

{-|


# Types

@docs Cursor


# Drag (item) functions

@docs dragStart, dragEnd


# Drop-target (inventory-box) functions

@docs dragEnter, dragLeave, dragOver, dragDrop

-}

import Dict exposing (Dict)
import Inventory.Location as Loc exposing (Location)


type alias Cursor a =
    { a
        | dragover : Maybe Location
        , dragstart : Maybe Location
    }


dragStart : Location -> Cursor a -> Cursor a
dragStart loc curs =
    { curs | dragstart = Just loc, dragover = Just loc }


dragEnd : Cursor a -> Cursor a
dragEnd curs =
    -- We're in trouble if this fires *after* InventoryDragDrop, which
    -- expects dragstart to be nonempty. Looks like the browser order is
    -- well-defined, though:
    -- https://stackoverflow.com/questions/38111946/is-there-a-defined-ordering-between-dragend-and-drop-events
    { curs | dragstart = Nothing, dragover = Nothing }


dragEnter : Location -> Cursor a -> Cursor a
dragEnter loc curs =
    { curs | dragover = Just loc }


dragOver : Location -> Cursor a -> Cursor a
dragOver loc curs =
    -- { curs | dragover = Just loc }
    curs


dragLeave : Location -> Cursor a -> Cursor a
dragLeave loc curs =
    if curs.dragover == Just loc then
        { curs | dragover = Nothing }

    else
        curs


dragDrop : Cursor a -> Cursor a
dragDrop curs =
    { curs | dragover = Nothing, dragstart = Nothing }
