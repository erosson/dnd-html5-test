module Inventory.Rect exposing
    ( Inventory, empty
    , getTab, hasTab, insertTab, removeTab
    , getItem, insertItem, removeItem, pushItem, swapItems, insertItems, quickpushItem
    )

{-| Inventory where items are two-dimensional rectangles.

Like Diablo, Path of Exile, or Grim Dawn. _Not_ like World of Warcraft or Eternium.

This inventory is more complex than the simpler 1x1 inventory implementation:
We need to track the maximum dimensions of each tab, and dimensions of each item.


# Inventory

@docs Inventory, empty


# Tabs

@docs getTab, hasTab, insertTab, removeTab


# Items

@docs getItem, insertItem, removeItem, updateItem, pushItem, swapItems, insertItems, quickpushItem

-}

import Dict exposing (Dict)
import Inventory.Location as Loc exposing (..)


type alias Item =
    { id : ItemId, size : Coords }


type alias Collider =
    { item : Item, origin : Coords }


type alias BoundedTab =
    { colliders : Dict Coords Collider
    , bounds : Coords
    }


type alias PublicTab =
    { items : Dict Coords Item
    , bounds : Coords
    }


createTab : Coords -> BoundedTab
createTab =
    BoundedTab Dict.empty


type alias Inventory =
    { tabs : Dict TabId BoundedTab
    , slots : Dict SlotName Item
    }


empty : Inventory
empty =
    { tabs = Dict.empty
    , slots = Dict.empty
    }


hasTab : TabId -> Inventory -> Bool
hasTab tab =
    .tabs >> Dict.member tab


getBoundedTab : TabId -> Inventory -> Maybe BoundedTab
getBoundedTab tab =
    .tabs >> Dict.get tab


getTab : TabId -> Inventory -> Maybe PublicTab
getTab tabId =
    getBoundedTab tabId >> Maybe.map toPublicTab


toPublicTab : BoundedTab -> PublicTab
toPublicTab tab =
    { bounds = tab.bounds
    , items =
        tab.colliders
            |> Dict.filter (\xy col -> xy == col.origin)
            |> Dict.map (always .item)
            |> Debug.log "topub"
    }


insertTab : TabId -> Coords -> Inventory -> Inventory
insertTab tabId bounds inv =
    if hasTab tabId inv then
        inv

    else
        { inv | tabs = inv.tabs |> Dict.insert tabId (createTab bounds) }


removeTab : TabId -> Inventory -> Inventory
removeTab tabId inv =
    { inv | tabs = inv.tabs |> Dict.remove tabId }


getItem : Location -> Inventory -> Maybe Item
getItem loc =
    case loc of
        Slot name ->
            .slots >> Dict.get name

        Tab tabId coords ->
            getCollider tabId coords >> Maybe.map .item


getCollider : TabId -> Coords -> Inventory -> Maybe Collider
getCollider tabId coords =
    getBoundedTab tabId
        >> Maybe.map .colliders
        >> Maybe.andThen (Dict.get coords)


colliderCoords : Coords -> Item -> List Coords
colliderCoords ( ox, oy ) { size } =
    let
        ( width, height ) =
            size

        xs =
            List.range ox (ox + width - 1)

        ys =
            List.range oy (oy + height - 1)
    in
    xs |> List.concatMap (\x -> ys |> List.map (Tuple.pair x))


insertColliders : Coords -> Item -> Dict Coords Collider -> Result String (Dict Coords Collider)
insertColliders origin item colliders =
    let
        coords =
            colliderCoords origin item

        collisions =
            coords |> List.filter (\pt -> Dict.member pt colliders)

        itemCollider =
            Collider item origin
    in
    case collisions of
        [] ->
            Ok <| List.foldl (\coord -> Dict.insert coord itemCollider) colliders coords

        collision :: _ ->
            Err <| "collisions at " ++ Debug.toString collisions


insertItem : Location -> Item -> Inventory -> Result String Inventory
insertItem loc item inv =
    case loc of
        Slot name ->
            case getItem loc inv of
                Just _ ->
                    Err "insertItem failed, slot is full"

                Nothing ->
                    Ok { inv | slots = inv.slots |> Dict.insert name item }

        Tab tabId (( x, y ) as coords) ->
            case getBoundedTab tabId inv of
                Nothing ->
                    Err "insertItem failed, no such tab"

                Just tab0 ->
                    -- bounds checking
                    let
                        ( xMax, yMax ) =
                            tab0.bounds

                        ( w, h ) =
                            item.size
                    in
                    if x < 0 || y < 0 || x + w - 1 >= xMax || y + h - 1 >= yMax then
                        Err "insertItem failed, out of bounds"

                    else
                        -- collision checking, update collision tables
                        case insertColliders coords item tab0.colliders of
                            Err err ->
                                Err <| "insertItem failed, " ++ err

                            Ok colliders ->
                                let
                                    tab =
                                        { tab0 | colliders = colliders }
                                in
                                Ok <| { inv | tabs = inv.tabs |> Dict.insert tabId tab }


removeItem : Location -> Inventory -> Inventory
removeItem loc inv =
    case loc of
        Slot name ->
            { inv | slots = inv.slots |> Dict.remove name }

        Tab tabId coords ->
            case getBoundedTab tabId inv of
                Nothing ->
                    inv

                Just tab0 ->
                    case getCollider tabId coords inv of
                        Nothing ->
                            inv

                        Just collider ->
                            let
                                colliders =
                                    colliderCoords collider.origin collider.item
                                        |> List.foldl Dict.remove tab0.colliders
                            in
                            { inv | tabs = inv.tabs |> Dict.insert tabId { tab0 | colliders = colliders } }


updateItem : Location -> Maybe Item -> Inventory -> Result String Inventory
updateItem loc mval =
    case mval of
        Nothing ->
            removeItem loc >> Ok

        Just val ->
            insertItem loc val


swapItems : Location -> Location -> Inventory -> Result String Inventory
swapItems loc1 loc2 inv =
    inv
        |> removeItem loc1
        |> removeItem loc2
        |> Ok
        |> Result.andThen (updateItem loc1 (getItem loc2 inv))
        |> Result.andThen (updateItem loc2 (getItem loc1 inv))


pushItem : TabId -> Item -> Inventory -> Result String Inventory
pushItem tabId item inv0 =
    -- Try coordinates for every location that could possibly work for an empty
    -- tab. Insert will check for collisions.
    case getBoundedTab tabId inv0 of
        Nothing ->
            Err "no such tab"

        Just tab ->
            let
                ( xMax, yMax ) =
                    tab.bounds

                ( w, h ) =
                    item.size

                xOriginMax =
                    xMax - w

                yOriginMax =
                    yMax - h

                loop : Int -> Int -> Result String Inventory
                loop x y =
                    if x > xOriginMax then
                        if y > yOriginMax then
                            Err "inventory is full"

                        else
                            loop 0 (y + 1)

                    else
                        case insertItem (Tab tabId ( x, y )) item inv0 of
                            Err _ ->
                                loop (x + 1) y

                            Ok inv ->
                                Ok inv
            in
            loop 0 0


insertItems : List ( Location, Item ) -> Inventory -> Result String Inventory
insertItems items inv =
    case items of
        [] ->
            Ok inv

        ( loc, item ) :: tail ->
            inv
                |> insertItem loc item
                |> Result.andThen (insertItems tail)


quickpushItem : Location -> TabId -> Inventory -> Result String Inventory
quickpushItem startLoc targetTabId inv =
    case getItem startLoc inv of
        Nothing ->
            Err "no item to transfer from that location"

        Just item ->
            inv
                |> removeItem startLoc
                |> pushItem targetTabId item
