module Inventory.Simple exposing
    ( Inventory, empty
    , getTab, hasTab, insertTab, removeTab
    , getItem, insertItem, removeItem, pushItem, swapItems, insertItems, quickpushItem
    )

{-| Inventory where items are two-dimensional rectangles.

Like Diablo, Path of Exile, or Grim Dawn. _Not_ like World of Warcraft or Eternium.

This inventory is more complex than the simpler 1x1 inventory implementation:
We need to track the maximum dimensions of each tab, and dimensions of each item.


# Inventory

@docs Inventory, empty


# Tabs

@docs getTab, hasTab, insertTab, removeTab


# Items

@docs getItem, insertItem, removeItem, updateItem, pushItem, swapItems, insertItems, quickpushItem

-}

import Dict exposing (Dict)
import Inventory.Location as Loc exposing (..)


type alias BoundedTab =
    { items : Dict Coords ItemId
    , bounds : Coords
    }


createTab : Coords -> BoundedTab
createTab =
    BoundedTab Dict.empty


type alias Inventory =
    { tabs : Dict TabId BoundedTab
    , slots : Dict SlotName ItemId
    }


empty : Inventory
empty =
    { tabs = Dict.empty
    , slots = Dict.empty
    }


hasTab : TabId -> Inventory -> Bool
hasTab tab =
    .tabs >> Dict.member tab


getBoundedTab : TabId -> Inventory -> Maybe BoundedTab
getBoundedTab tab =
    .tabs >> Dict.get tab


getTab : TabId -> Inventory -> Maybe BoundedTab
getTab tabId =
    -- getBoundedTab tabId >> Maybe.map .items
    getBoundedTab tabId


insertTab : TabId -> Coords -> Inventory -> Inventory
insertTab tabId bounds inv =
    if hasTab tabId inv then
        inv

    else
        { inv | tabs = inv.tabs |> Dict.insert tabId (createTab bounds) }


removeTab : TabId -> Inventory -> Inventory
removeTab tabId inv =
    { inv | tabs = inv.tabs |> Dict.remove tabId }


getItem : Location -> Inventory -> Maybe ItemId
getItem loc =
    case loc of
        Slot name ->
            .slots >> Dict.get name

        Tab tabId coords ->
            getTab tabId >> Maybe.andThen (.items >> Dict.get coords)


insertItem : Location -> ItemId -> Inventory -> Result String Inventory
insertItem loc item inv =
    case loc of
        Slot name ->
            case getItem loc inv of
                Just _ ->
                    Err "insertItem failed, slot is full"

                Nothing ->
                    Ok { inv | slots = inv.slots |> Dict.insert name item }

        Tab tabId (( x, y ) as coords) ->
            case getBoundedTab tabId inv of
                Nothing ->
                    Err "insertItem failed, no such tab"

                Just tab0 ->
                    if Dict.member coords tab0.items then
                        Err "insertItem failed, collision"

                    else
                        let
                            ( xMax, yMax ) =
                                tab0.bounds
                        in
                        if x < 0 || y < 0 || x >= xMax || y >= yMax then
                            Err "insertItem failed, out of bounds"

                        else
                            let
                                items =
                                    tab0.items |> Dict.insert coords item
                            in
                            Ok { inv | tabs = inv.tabs |> Dict.insert tabId { tab0 | items = items } }


removeItem : Location -> Inventory -> Inventory
removeItem loc inv =
    case loc of
        Slot name ->
            { inv | slots = inv.slots |> Dict.remove name }

        Tab tabId coords ->
            { inv | tabs = inv.tabs |> Dict.update tabId (Maybe.map (\t -> { t | items = t.items |> Dict.remove coords })) }


updateItem : Location -> Maybe ItemId -> Inventory -> Result String Inventory
updateItem loc mval =
    case mval of
        Nothing ->
            removeItem loc >> Ok

        Just val ->
            insertItem loc val


swapItems : Location -> Location -> Inventory -> Result String Inventory
swapItems loc1 loc2 inv =
    inv
        |> removeItem loc1
        |> removeItem loc2
        |> Ok
        |> Result.andThen (updateItem loc1 (getItem loc2 inv))
        |> Result.andThen (updateItem loc2 (getItem loc1 inv))


pushItem : TabId -> ItemId -> Inventory -> Result String Inventory
pushItem tabId item inv0 =
    -- Try coordinates for every location that could possibly work for an empty
    -- tab. Insert will check for collisions.
    case getBoundedTab tabId inv0 of
        Nothing ->
            Err "no such tab"

        Just tab ->
            let
                ( xMax, yMax ) =
                    tab.bounds

                loop : Int -> Int -> Result String Inventory
                loop x y =
                    if x >= xMax then
                        if y >= yMax then
                            Err "inventory is full"

                        else
                            loop 0 (y + 1)

                    else
                        case insertItem (Tab tabId ( x, y )) item inv0 of
                            Err _ ->
                                loop (x + 1) y

                            Ok inv ->
                                Ok inv
            in
            loop 0 0


insertItems : List ( Location, ItemId ) -> Inventory -> Result String Inventory
insertItems items inv =
    case items of
        [] ->
            Ok inv

        ( loc, item ) :: tail ->
            inv
                |> insertItem loc item
                |> Result.andThen (insertItems tail)


quickpushItem : Location -> TabId -> Inventory -> Result String Inventory
quickpushItem startLoc targetTabId inv =
    case getItem startLoc inv of
        Nothing ->
            Err "no item to transfer from that location"

        Just item ->
            inv
                |> removeItem startLoc
                |> pushItem targetTabId item
