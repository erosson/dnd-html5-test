module Inventory.Location exposing (..)

import Dict exposing (Dict)


type Location
    = Tab TabId Coords
    | Slot SlotName


toString : Location -> String
toString loc =
    case loc of
        Tab tabId ( x, y ) ->
            [ "tab", tabId, String.fromInt x, String.fromInt y ]
                |> String.join ","

        Slot name ->
            "slot," ++ name


type alias TabId =
    String


type alias Coords =
    ( Int, Int )


type alias SlotName =
    String


type alias ItemId =
    String


type alias Tab =
    Dict Coords ItemId
